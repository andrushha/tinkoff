import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by andrushha on 03.07.2016.
 */
public class TestApi {

    private static final String URL = "https://www.tinkoff.ru/api/v1/currency_rates/";
    HttpClient client;
    HttpGet request;
    HttpResponse response;
    Gson gson;
    CurrencyRatesWrapper currencyRates;
    BufferedReader bufferedReader;

    @Before
    public void setUp() throws Exception {
        client = new DefaultHttpClient();
        request = new HttpGet(URL);
        gson = new Gson();
        response = client.execute(request);
        bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }

        currencyRates = gson.fromJson(sb.toString(), CurrencyRatesWrapper.class);
    }


    @Test
    public void testToCurrencyNameMapping() throws Exception {


        Rates[] rates = currencyRates.getPayload().getRates();

        for (int i = 0; i < rates.length; i++) {
            int code = rates[i].getToCurrency().getCode();
            String name = rates[i].getToCurrency().getName();
            switch (code) {
                case 643:
                    Assert.assertEquals("RUB", name);
                    break;
                case 840:
                    Assert.assertEquals("USD", name);
                    break;
                case 978:
                    Assert.assertEquals("EUR", name);
                    break;
                case 826:
                    Assert.assertEquals("GBR", name);
                    break;
                default:
                    break;
            }
        }
    }

    @Test
    public void testFromCurrencyNameMapping() throws Exception {

        Rates[] rates = currencyRates.getPayload().getRates();

        for (int i = 0; i < rates.length; i++) {
            int code = rates[i].getFromCurrency().getCode();
            String name = rates[i].getFromCurrency().getName();
            switch (code) {
                case 643:
                    Assert.assertEquals("RUB", name);
                    break;
                case 840:
                    Assert.assertEquals("USD", name);
                    break;
                case 978:
                    Assert.assertEquals("EUR", name);
                    break;
                case 826:
                    Assert.assertEquals("GBR", name);
                    break;
                default:
                    break;
            }
        }
    }

    @After
    public void tearDown() throws Exception{
        bufferedReader.close();
    }

}

