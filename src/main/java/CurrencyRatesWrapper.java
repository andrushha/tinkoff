/**
 * Created by andrushha on 03.07.2016.
 */
public class CurrencyRatesWrapper {

    private String resultCode;
    private Payload payload;

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }


    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }
}

class Payload {
    private LastUpdate lastUpdate;
    private Rates[] rates;

    public LastUpdate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LastUpdate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Rates[] getRates() {
        return rates;
    }

    public void setRates(Rates[] rates) {
        this.rates = rates;
    }
}

class LastUpdate {
    private long milliseconds;

    public long getMilliseconds() {
        return milliseconds;
    }

    public void setMilliseconds(long milliseconds) {
        this.milliseconds = milliseconds;
    }
}

class Rates {

    private String category;
    private double buy;
    private double sell;
    private FromCurrency fromCurrency;
    private ToCurrency toCurrency;

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public FromCurrency getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(FromCurrency fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public ToCurrency getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(ToCurrency toCurrency) {
        this.toCurrency = toCurrency;
    }


}


class FromCurrency {

    private int code;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}


class ToCurrency {

    private int code;
    private String name;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
